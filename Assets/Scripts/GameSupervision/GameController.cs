﻿using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameSupervision
{
    public class GameController : MonoBehaviour
    {
        public GameObject standardAsteroid;
        public GameObject bouncingAsteroid;

        private int _score;
        private int _hiscore;
        private int _asteroidsRemaining;
        private int _lives;
        private int _waves;
        private int _asteroidIncrease = 4;
        private XmlDocument _config = new XmlDocument();
        
        public Text scoreText;
        public Text livesText;
        public Text waveText;
        public Text hiscoreText;

        private void Start()
        {
            _hiscore = PlayerPrefs.GetInt("_hiscore", 0);
            BeginGame();
        }

        private void Update()
        {
            if (Input.GetKey("escape"))
            {
                Application.Quit();
            }
        }

        private void BeginGame()
        {
            _config.Load("Assets/Scripts/Config/config.xml");
            XmlElement xRoot = _config.DocumentElement;
            if (xRoot != null)
            {
                foreach (XmlNode xnode in xRoot)
                {
                    foreach (XmlNode childnode in xnode.ChildNodes)
                    {
                        if (childnode.Name == "lives")
                        {
                            _lives = int.Parse(childnode.InnerText);
                        }   
                    }
                }
            }
            _score = 0;
            _waves = 1;

            scoreText.text = "Score: " + _score;
            livesText.text = "Lives: " + _lives;
            waveText.text = "Waves: " + _waves;

            SpawnAsteroids();
        }

        private void SpawnAsteroids()
        {
            DestroyExistingAsteroids();
            DestroyExistingPowerUps();
            
            _asteroidsRemaining = _waves * _asteroidIncrease;
        
            float sceneXSize = Camera.main.orthographicSize + 1;
            float sceneYSize = Camera.main.orthographicSize + 1;
        
            for (int i = 0; i < _asteroidsRemaining; i++)
            {
                int asteroidType = Random.Range(0, 2);

                if (asteroidType == 0)
                {
                    Instantiate(
                        standardAsteroid,
                        new Vector3(
                            Random.Range(-sceneXSize, sceneXSize), 
                            Random.Range(-sceneYSize, sceneYSize), 
                            0
                        ),
                        Quaternion.Euler(0, 0, Random.Range(-0.0f, 359.0f))
                    );
                }
                else if (asteroidType == 1)
                {
                    Instantiate(
                        bouncingAsteroid,
                        new Vector3(
                            Random.Range(-sceneXSize, sceneXSize), 
                            Random.Range(-sceneYSize, sceneYSize), 
                            0
                        ),
                        Quaternion.Euler(0, 0, Random.Range(-0.0f, 359.0f))
                    );
                }
            }

            waveText.text = "Wave: " + _waves;
        }

        private void DestroyExistingAsteroids()
        {
            GameObject[] largeAsteroids = GameObject.FindGameObjectsWithTag("Large Asteroid");
            foreach (var current in largeAsteroids)
            {
                GameObject.Destroy(current);
            }

            GameObject[] bouncingAsteroids = GameObject.FindGameObjectsWithTag("Bouncing Asteroid");
            foreach (var current in bouncingAsteroids)
            {
                GameObject.Destroy(current);
            }
        
            GameObject[] smallAsteroids = GameObject.FindGameObjectsWithTag("Small Asteroid");
            foreach (var current in smallAsteroids)
            {
                GameObject.Destroy(current);
            }
        }

        private void DestroyExistingPowerUps()
        {
            GameObject[] livePowerUps = GameObject.FindGameObjectsWithTag("LivePowerUp");
            foreach (var powerUp in livePowerUps)
            {
                GameObject.Destroy(powerUp);
            }
        }

        public void IncrementScore()
        {
            _score++;

            scoreText.text = "Score: " + _score;

            if (_score > _hiscore)
            {
                _hiscore = _score;
                hiscoreText.text = "Hiscore: " + _hiscore;
                PlayerPrefs.SetInt("_hiscore", _hiscore);
            }

            if (_asteroidsRemaining < 1)
            {
                _waves++;
                SpawnAsteroids();
            }
        }

        public void DecrementLives()
        {
            _lives--;
            livesText.text = "Lives: " + _lives;

            if (_lives < 1)
            {
                BeginGame();
            }
        }
        
        public void IncrementLives()
        {
            _lives++;
            livesText.text = "Lives: " + _lives;
        }

        public void DecrementAsteroids()
        {
            _asteroidsRemaining--;
        }

        public void SpiltAsteroids()
        {
            _asteroidsRemaining += 2;
        }
    }
}