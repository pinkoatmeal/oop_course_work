﻿using UnityEngine;

namespace GameSupervision
{
    public class EuclideanTorus : MonoBehaviour
    {
        private void Update()
        {
            float sceneXSize = Camera.main.orthographicSize * Camera.main.aspect + 1;
            float sceneYSize = Camera.main.orthographicSize + 1;
        
            if (transform.position.x > sceneXSize)
            {
                transform.position = new Vector3(-sceneXSize, transform.position.y, 0);
            }
            else if (transform.position.x < -sceneXSize)
            {
                transform.position = new Vector3(sceneXSize, transform.position.y, 0);
            }
            else if (transform.position.y > sceneYSize)
            {
                transform.position = new Vector3(transform.position.x, -sceneYSize, 0);
            }
            else if (transform.position.y < -sceneYSize)
            {
                transform.position = new Vector3(transform.position.x, sceneYSize, 0);
            }
        }
    }
}
