﻿using UnityEngine;

namespace Weapons
{
    public class BulletController : MonoBehaviour
    {
        private void Start()
        {
            Destroy(gameObject, 1.0f);
        
            GetComponent<Rigidbody2D>().AddForce(transform.up * 400);
        }
    }
}
