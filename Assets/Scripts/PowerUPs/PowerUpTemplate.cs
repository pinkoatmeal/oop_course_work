﻿using System;
using GameSupervision;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PowerUPs
{
    public abstract class PowerUpTemplate : MonoBehaviour
    {
        public AudioClip spawnSound;
        public AudioClip pickUpSound;
        public AudioClip destroySound;
        
        protected GameController gameController;
        
        private void Start()
        {
            GameObject gameControllerObject = GameObject.FindWithTag("GameController");
            gameController = gameControllerObject.GetComponent<GameController>();

            GetComponent<Rigidbody2D>().AddForce(transform.up * Random.Range(-10.0f, 10.0f));
            
            GetComponent<Rigidbody2D>().angularVelocity = Random.Range(1.0f, 20.0f);
        }

        protected abstract void OnCollisionEnter2D(Collision2D other);
        protected abstract void OnTriggerEnter2D(Collider2D other);
    }
}