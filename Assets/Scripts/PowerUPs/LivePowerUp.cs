﻿using System;
using UnityEngine;

namespace PowerUPs
{
    public class LivePowerUp : PowerUpTemplate
    {
        protected override void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.CompareTag("Bullet"))
            {
                Destroy(other.gameObject);
                Destroy(gameObject);
                AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position);
            }
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Ship"))
            {
                Destroy(gameObject);
                
                AudioSource.PlayClipAtPoint(pickUpSound, Camera.main.transform.position);
                gameController.IncrementLives();
            }
        }
    }
}