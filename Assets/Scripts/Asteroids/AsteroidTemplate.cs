﻿using System;
using GameSupervision;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Asteroids
{
    public abstract class AsteroidTemplate : MonoBehaviour
    {
        private protected virtual int AsteroidHp { get; set; } = 1;
        
        public AudioClip destroySound;
        public AudioClip shipCrashSound;

        protected GameController gameController;

        private void Start()
        {
            GameObject gameControllerObject = GameObject.FindWithTag("GameController");
            gameController = gameControllerObject.GetComponent<GameController>();

            GetComponent<Rigidbody2D>().AddForce(transform.up * Random.Range(-50.0f, 150.0f));
            
            GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-0.0f, 90.0f);
        }

        protected abstract void OnTriggerEnter2D(Collider2D other); // For ship collisions
        protected abstract void OnCollisionEnter2D(Collision2D other); // For other collisions
        
    }
}