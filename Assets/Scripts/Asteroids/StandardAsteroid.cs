﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Asteroids
{
    public class StandardAsteroid : AsteroidTemplate
    {
        public GameObject smallAsteroid;
        public GameObject livePowerUp;

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Ship"))
            {
                AudioSource.PlayClipAtPoint(shipCrashSound, Camera.main.transform.position);

                other.gameObject.transform.position = new Vector3(0, 0, 0);
                other.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
                other.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0.0f;

                gameController.DecrementLives();
            }
        }

        protected override void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag.Equals("Bullet"))
            {
                Destroy(other.gameObject);
                
                Instantiate(
                    smallAsteroid, 
                    new Vector3(transform.position.x - .9f, transform.position.y - .9f, 0),
                    Quaternion.Euler(0, 0, 90)
                );
                Instantiate(
                    smallAsteroid, 
                    new Vector3(transform.position.x + .9f, transform.position.y + .9f, 0),
                    Quaternion.Euler(0, 0, 0)
                );
                Instantiate(
                    smallAsteroid, 
                    new Vector3(transform.position.x + .9f, transform.position.y - .9f, 0),
                    Quaternion.Euler(0, 0, 270)
                );

                if (Random.Range(-2, 2) > 0)
                {
                    Instantiate(
                        livePowerUp, 
                        new Vector3(transform.position.x, transform.position.y, 0),
                        Quaternion.Euler(0, 0, 0)
                    );
                }
                
                gameController.SpiltAsteroids();
                
                AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position);
                Destroy(gameObject);
            }
        }
    }
}