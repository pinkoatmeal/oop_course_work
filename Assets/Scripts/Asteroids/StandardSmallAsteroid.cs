﻿using UnityEngine;

namespace Asteroids
{
    public class StandardSmallAsteroid : AsteroidTemplate
    {
        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Ship"))
            {
                AudioSource.PlayClipAtPoint(shipCrashSound, Camera.main.transform.position);

                other.gameObject.transform.position = new Vector3(0, 0, 0);
                other.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
                other.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0.0f;

                gameController.DecrementLives();
            }
        }

        protected override void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag.Equals("Bullet"))
            {
                Destroy(other.gameObject);
                
                AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position);
                
                gameController.DecrementAsteroids();
                gameController.IncrementScore();
                
                Destroy(gameObject);
            }
        }
    }
}