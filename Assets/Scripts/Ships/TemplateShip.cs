﻿using GameSupervision;
using UnityEngine;

namespace Ships
{
    public abstract class TemplateShip : MonoBehaviour
    {
        private protected virtual float RotationSpeed { get; set; } = 80.0f;
        private protected virtual float ThrustForce { get; set; } = 3.0f;

        public AudioClip crashSound;
        public AudioClip shootSound;

        protected GameController gameController;

        private void Start()
        {
            GameObject gameControllerObject = GameObject.FindWithTag("GameController");
            gameController = gameControllerObject.GetComponent<GameController>();
        }

        protected virtual void FixedUpdate()
        {
            transform.Rotate(0, 0, -Input.GetAxis("Horizontal") * RotationSpeed * Time.deltaTime);
        
            GetComponent<Rigidbody2D>().AddForce(Input.GetAxis("Vertical") * ThrustForce * transform.up);
        }

        // private void OnTriggerEnter2D(Collider2D other)
        // {
        //     if (!other.gameObject.CompareTag("Bullet"))
        //     {
        //         AudioSource.PlayClipAtPoint(crashSound, Camera.main.transform.position);
        //     
        //         transform.position = new Vector3(0, 0, 0);
        //         GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
        //     
        //         _gameController.DecrementLives();
        //     }
        // }
        
        protected abstract void ShootBullet();
    }
}