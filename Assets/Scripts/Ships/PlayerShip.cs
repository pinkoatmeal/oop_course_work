﻿using System;
using UnityEngine;

namespace Ships
{
    internal class PlayerShip : TemplateShip
    {
        private protected override float RotationSpeed { get; set; } = 100.0f;
        private protected override float ThrustForce { get; set; } = 3.0f;

        public GameObject bullet;

        protected override void FixedUpdate()
        {
            transform.Rotate(0, 0, -Input.GetAxis("Horizontal") * RotationSpeed * Time.deltaTime);
            GetComponent<Rigidbody2D>().AddForce(Input.GetAxis("Vertical") * ThrustForce * transform.up);

            if (Input.GetMouseButtonDown(0))
            {
                ShootBullet();
            }
        }

        protected override void ShootBullet()
        {
            Instantiate(bullet, new Vector3(transform.position.x, transform.position.y), transform.rotation);
            
            AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position);
        }
    }
    
}