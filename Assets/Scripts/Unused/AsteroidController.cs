﻿using GameSupervision;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public AudioClip destroySound;
    public GameObject smallAsteroid;

    private GameController _gameController;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        _gameController = gameControllerObject.GetComponent<GameController>();
        
        GetComponent<Rigidbody2D>().AddForce(transform.up * Random.Range(-50.0f, 150.0f));

        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-0.0f, 90.0f);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Bullet"))
        {
            Destroy(other.gameObject);

            if (tag.Equals("Large Asteroid"))
            {
                Instantiate(
                    smallAsteroid, 
                    new Vector3(transform.position.x - .5f, transform.position.y - .5f, 0),
                    Quaternion.Euler(0, 0, 90)
                );
                Instantiate(
                    smallAsteroid, 
                    new Vector3(transform.position.x + .5f, transform.position.y + .5f, 0),
                    Quaternion.Euler(0, 0, 0)
                );
                Instantiate(
                    smallAsteroid, 
                    new Vector3(transform.position.x + .5f, transform.position.y - .5f, 0),
                    Quaternion.Euler(0, 0, 270)
                );
                
                _gameController.SpiltAsteroids();
            }
            AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position);
            _gameController.IncrementScore();
            Destroy(gameObject);
        }
    }
}