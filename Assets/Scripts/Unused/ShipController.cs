﻿using System;
using GameSupervision;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    private const float RotationSpeed = 100.0f;
    private const float ThrustForce = 3f;

    public AudioClip crashSound;
    public AudioClip shootSound;

    public GameObject bullet;

    private GameController _gameController;

    private void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        _gameController = gameControllerObject.GetComponent<GameController>();
    }

    private void FixedUpdate()
    {
        transform.Rotate(0, 0, -Input.GetAxis("Horizontal") * RotationSpeed * Time.deltaTime);
        
        GetComponent<Rigidbody2D>().AddForce(Input.GetAxis("Vertical") * ThrustForce * transform.up);

        if (Input.GetMouseButtonDown(0))
        {
            ShootBullet();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Bullet"))
        {
            AudioSource.PlayClipAtPoint(crashSound, Camera.main.transform.position);
            
            transform.position = new Vector3(0, 0, 0);
            GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);
            
            _gameController.DecrementLives();
        }
    }

    private void ShootBullet()
    {
        Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
        
        AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position);
    }
}